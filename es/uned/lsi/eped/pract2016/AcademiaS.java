package es.uned.lsi.eped.pract2016;

import es.uned.lsi.eped.DataStructures.IteratorIF;
import es.uned.lsi.eped.DataStructures.List;
import es.uned.lsi.eped.DataStructures.ListIF;
import es.uned.lsi.eped.DataStructures.Tree;
import es.uned.lsi.eped.DataStructures.TreeIF;

public class AcademiaS implements AcademiaIF {
	
	//Atributos
	private TreeIF<DoctorIF> academiaTree;

	/*CONSTRUCTOR PRUEBAS
	public AcademiaS() {
		this.academiaTree = new Tree<DoctorIF>(new DoctorS(1));
		Tree<DoctorIF> doc2 = new Tree<DoctorIF>(new DoctorS(2));
		Tree<DoctorIF> doc3 = new Tree<DoctorIF>(new DoctorS(3));
		Tree<DoctorIF> doc4 = new Tree<DoctorIF>(new DoctorS(4));
		Tree<DoctorIF> doc5 = new Tree<DoctorIF>(new DoctorS(5));
		Tree<DoctorIF> doc6 = new Tree<DoctorIF>(new DoctorS(6));
		Tree<DoctorIF> doc7 = new Tree<DoctorIF>(new DoctorS(7));
		Tree<DoctorIF> doc8 = new Tree<DoctorIF>(new DoctorS(8));
		//6 hijo8
		doc6.addChild(doc6.getChildren().size() +1, doc8);
		//5 hijo 7 
		doc5.addChild(doc5.getChildren().size() +1, doc7);
		//3 hijos 5  y 6
		doc3.addChild(doc3.getChildren().size() +1, doc5);
		doc3.addChild(doc3.getChildren().size() +1, doc6);
		//padre 2, hijo 4
		doc2.addChild(doc2.getChildren().size() +1, doc4);
		//padre 1, hijos 2 , 3
		this.academiaTree.addChild(this.academiaTree.getChildren().size()+1, doc2);
		this.academiaTree.addChild(this.academiaTree.getChildren().size()+1, doc3);
		
	}*/
	//Constructores
	public AcademiaS(DoctorIF founder) {
		this.academiaTree = new Tree<DoctorIF>(founder);
	}
	
	public AcademiaS() {
		academiaTree = new Tree<DoctorIF>();
	}
	//fin constructores
	@Override
	public boolean isEmpty() {
		return this.academiaTree.isEmpty();
	}

	@Override
	public boolean contains(DoctorIF e) {
		return this.academiaTree.contains((DoctorS)e);
	}

	@Override
	public void clear() {
		this.academiaTree.clear();
	}

	@Override
	public IteratorIF<DoctorIF> iterator() {
		return this.academiaTree.iterator();
	}

	@Override
	public DoctorIF getFounder() {
		return this.academiaTree.getRoot();
	}
	
	@Override
	public DoctorIF getDoctor(int id) {
		return this.getDoctorTree(this.academiaTree, id).getRoot();
	}
	public TreeIF<DoctorIF> getDoctorTree(int id) {
		return getDoctorTree(this.academiaTree, id);
	}
	//metodo para obtener el arbol de un doctor
	private TreeIF<DoctorIF> getDoctorTree(TreeIF<DoctorIF> arbol, int id) {
		//caso base, termina las llamadas recursivas, hemos encontrado el arbol del doctor
		if(arbol.getRoot().getID() == id) return arbol;
		
		//buscamos en los hijos
		ListIF<TreeIF<DoctorIF>> hijos = arbol.getChildren();
		IteratorIF<TreeIF<DoctorIF>> it = hijos.iterator();
		//bucle con llamada recursiva
		while(it.hasNext()) {
			//llamada recursiva
			TreeIF<DoctorIF> arbolActual = getDoctorTree(it.getNext(), id);
			//si no es nulo, significa que lo hemos encontrado en nuestra academia
			if(arbolActual != null)
				return arbolActual;
		}
		//si al terminar el bucle no hemos encontrado al doctor devolvera nulo.
		return null;
	}
	@Override
	public int size() {
		return this.academiaTree.size();
	}

	@Override
	public void addDoctor(DoctorIF newDoctor, DoctorIF supervisor) {
		// Comprobamos que el fundador existe en la academia, si no, estaremos añadiendo
		//al fundador de la misma
		if(this.getFounder() == null) {
			this.academiaTree.setRoot(newDoctor);
		}
		//buscamos arbol supervisor
		TreeIF<DoctorIF> arbolSupervisor = this.getDoctorTree(this.academiaTree, supervisor.getID());
		//nuevo arbol del estudiante, deberiamos comprobar si existe?
		TreeIF<DoctorIF> hijo = new Tree<DoctorIF>(newDoctor);
		//añadimos el hijo
		arbolSupervisor.addChild(arbolSupervisor.getChildren().size()+1, hijo);
	}

	@Override
	public void addSupervision(DoctorIF student, DoctorIF supervisor) {
		//No implementado, no tiene sentido en este escenario ya que solo se admite 1 director de tesis.
	}
}
