package es.uned.lsi.eped.pract2016;

import es.uned.lsi.eped.DataStructures.CollectionIF;
import es.uned.lsi.eped.DataStructures.IteratorIF;
import es.uned.lsi.eped.DataStructures.List;
import es.uned.lsi.eped.DataStructures.ListIF;
import es.uned.lsi.eped.DataStructures.Tree;
import es.uned.lsi.eped.DataStructures.TreeIF;

public class DoctorS implements DoctorIF {
	//Atributos
	private int ID;
	private DoctorS supervisor;
	private AcademiaS academia;
	//Constructor
	public DoctorS(int ID) {
		this.ID = ID;
	}
	public DoctorS() {}
	public DoctorS(int ID, AcademiaIF a, DoctorIF doctor) {
		this.ID = ID;
		this.academia = (AcademiaS) a;
		this.supervisor = (DoctorS) doctor;
	}
	//
	public DoctorS getSupervisor() {
		return this.supervisor;
	}
	
	public void setSupervisor(DoctorS supervisor) {
		this.supervisor = supervisor;
	}
	public void setAcademia(AcademiaS a) {
		this.academia = a;
	}
	@Override
	public CollectionIF<DoctorIF> getAncestors(int generations) {
		//Comprobamos que generations tiene un valor mayor de 0, si es así llamamos al metodo recursivo empezando por el
		//supervisor del doctor llamante
		if(generations > 0 && !(this.equals(this.academia.getFounder()))) 
			return getAncestorsR(new List<DoctorIF>(), this.supervisor, --generations);
		return null;
	}
	private ListIF<DoctorIF> getAncestorsR(ListIF<DoctorIF> lAncestors, DoctorIF actual, int generations) {
		//Añadimos el supervisor a la lista
		lAncestors.insert(actual, 1);
		//caso base, comprobamos si hemos terminado, si el doctor(supervisor) actual es la raiz de la academia, o si generations
		//es 0.
		if(actual.equals(this.academia.getFounder()) || generations == 0) {
			return lAncestors;
		}
		//recursivo
		DoctorS supervisorSiguiente = ((DoctorS)actual).getSupervisor();//guardamos el supervisor del supervisor Actual 
		return getAncestorsR(lAncestors, supervisorSiguiente, --generations);
	}
	@Override
	public CollectionIF<DoctorIF> getStudents() {
		//Guardamos la lista de arboles con los hijos
		ListIF<TreeIF<DoctorIF>> listaTree = this.academia.getDoctorTree(this.ID).getChildren();
		//llamamos al metodo que pasa la lista de arboles a lista de raices y retornamos.
		return treeToRoot(listaTree);
	}

	@Override
	public CollectionIF<DoctorIF> getDescendants(int generations) {
		//Obtener el arbol del doctor llamante
		TreeIF<DoctorIF> doc = this.academia.getDoctorTree(this.ID);//Obtenemos el arbol del doctor
		return getDescendantsR(new List<DoctorIF>(),doc.getChildren(), generations);
	}
	public ListIF<DoctorIF> getDescendantsR(ListIF<DoctorIF> lDescent, ListIF<TreeIF<DoctorIF>> actual, int generations) {
		//caso base
		if(generations == 0)
			return lDescent;
		else
			--generations;
		//Siguiente generacion
		List<TreeIF<DoctorIF>> siguienteGen = new List<TreeIF<DoctorIF>>();
		//Recorremos la lista de hijos actuales para meterlos en nuestra lista de descendientes
		IteratorIF<TreeIF<DoctorIF>> it = actual.iterator();
		//Añadimos la generacion a la lista
		while(it.hasNext()) {
			TreeIF<DoctorIF> doc = it.getNext();
			lDescent.insert(doc.getRoot(), 1);
			//Si tiene hijos los agregamos a la siguiente generacion
			if(!doc.isLeaf()) {
				IteratorIF<TreeIF<DoctorIF>> itNextG = doc.getChildren().iterator();
				
				while(itNextG.hasNext()) {
					siguienteGen.insert(itNextG.getNext(), 1);
				}
			}
		}
		return lDescent = getDescendantsR(lDescent, siguienteGen, generations);
	}
	@Override
	public CollectionIF<DoctorIF> getSiblings() {
		//lista de hermanos
		ListIF<DoctorIF> siblingsList = new List<DoctorIF>();
		// Buscamos el arbol del supervisor
		ListIF<TreeIF<DoctorIF>> listaTree = this.academia.getDoctorTree(this.supervisor.getID()).getChildren();
		//eliminamos el doctor llamante de la lista
		IteratorIF<TreeIF<DoctorIF>> it = listaTree.iterator();
		
		while(it.hasNext()) {
			DoctorIF doc = it.getNext().getRoot();
			//si no es este doctor lo añadimos a la lista que devolveremos
			if(!(doc.getID() == this.ID)) {
				siblingsList.insert(doc, 1);
			}
		}
		return siblingsList;
	}

	@Override
	public int getID() {
		return this.ID;
	}
	
	//metodo auxiliar para pasar una lista de arboles a una lista de raices
	private ListIF<DoctorIF> treeToRoot(CollectionIF<TreeIF<DoctorIF>> lista) {
		IteratorIF<TreeIF<DoctorIF>> it = lista.iterator();
		ListIF<DoctorIF> docList = new List<DoctorIF>();
		while(it.hasNext()) {
			docList.insert(it.getNext().getRoot(), 1);
		}
		return docList;
	}
}
